package com.tumblrdecode.model;

import com.google.gson.annotations.SerializedName;

public class AltSize {

    @SerializedName("width")
    public int width;

    @SerializedName("height")
    public int height;

    @SerializedName("url")
    public String url;

}
