package com.tumblrdecode.interfaces;

public interface OnBottomReachedListener {
    void onBottomReached(int position);
}
