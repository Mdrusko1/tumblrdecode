package com.tumblrdecode.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tumblrdecode.R;
import com.tumblrdecode.adapter.RecycleViewAdapter;
import com.tumblrdecode.api.ApiClient;
import com.tumblrdecode.api.ApiInterface;
import com.tumblrdecode.helpers.DateFunction;
import com.tumblrdecode.interfaces.OnBottomReachedListener;
import com.tumblrdecode.model.Data;
import com.tumblrdecode.model.Post;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tumblrdecode.App.API_KEY;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_blog_title)
    TextView txtBlogTitle;
    @BindView(R.id.txt_total_posts)
    TextView txtTotalPosts;
    @BindView(R.id.txt_updated)
    TextView txtUpdated;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private List<Post> postList = new ArrayList<>();
    private RecycleViewAdapter adapter;
    private int pageIndex = 0;
    private int pageLimit = 20;
    private int totalPosts;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_scrolling);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.app_name);

        // decorate status bar
        createTranparentStatusBar();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new RecycleViewAdapter(this, postList);
        recyclerView.setAdapter(adapter);

        firstLoad();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                postList.clear();
                firstLoad();
            }
        });


        adapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                if (pageIndex < totalPosts) {
                    loadNextPage();
                }
            }
        });
    }


    private void firstLoad() {
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Data> call = apiService.getPosts(API_KEY, pageIndex, pageLimit);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                List<Post> results = fetchResults(response);
                postList.addAll(results);
                totalPosts = response.body().response.blog.total_posts;
                adapter.setPostList(postList);
                swipeRefreshLayout.setRefreshing(false);

                // set header data
                txtBlogTitle.setText(response.body().response.blog.title);
                txtTotalPosts.setText(String.valueOf(response.body().response.blog.total_posts));
                txtUpdated.setText(DateFunction.getDateCurrentTimeZone(Long.parseLong(response.body().response.blog.updated)));

                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                // handle error
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Log.d("Error", t.getMessage());
            }
        });
    }


    private void loadNextPage() {
        final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        pageIndex = pageIndex + pageLimit;
        Call<Data> call = apiService.getPosts(API_KEY, pageIndex, pageLimit);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                List<Post> results = fetchResults(response);
                postList.addAll(results);
                adapter.setPostList(postList);
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                Log.d("TAG", "Response failure = " + t.toString());
            }
        });
    }


    private List<Post> fetchResults(Response<Data> response) {
        Data data = response.body();
        return data.response.posts;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_back_to_exit, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    // decoration
    private void createTranparentStatusBar() {
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        ViewGroup contentParent = findViewById(android.R.id.content);
        View content = contentParent.getChildAt(0);
        setFitsSystemWindows(content, false, true);
        clipToStatusBar(toolbar);
    }

    protected void setFitsSystemWindows(View view, boolean fitSystemWindows, boolean applyToChildren) {
        if (view == null) return;
        view.setFitsSystemWindows(fitSystemWindows);
        if (applyToChildren && (view instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0, n = viewGroup.getChildCount(); i < n; i++) {
                viewGroup.getChildAt(i).setFitsSystemWindows(fitSystemWindows);
            }
        }
    }

    protected void clipToStatusBar(View view) {
        final int statusBarHeight = getStatusBarHeight(this);
        view.getLayoutParams().height += statusBarHeight;
        view.setPadding(0, statusBarHeight, 0, 0);
    }

    protected int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}


