package com.tumblrdecode.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tumblrdecode.R;
import com.tumblrdecode.helpers.DateFunction;
import com.tumblrdecode.model.Post;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {
    @BindView(R.id.img_post_large)
    ImageView imgPostLarge;
    @BindView(R.id.txt_title_details)
    TextView txtTitleDetails;
    @BindView(R.id.view_separator)
    View viewSeparator;
    @BindView(R.id.txt_paracelable_data)
    TextView txtParacelableData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        setupToolbar();

        // get paracelable data
        Post post = getIntent().getParcelableExtra("Post");
        if (post != null) {

            try {
                Objects.requireNonNull(getSupportActionBar()).setTitle(post.summary);
                txtTitleDetails.setText(post.summary);

                String tags = "";
                for (int i = 0; i < post.tags.size(); i++) {
                    tags = tags + " #" + post.tags.get(i);
                }

                String data = "<b> Blog name:</b> " + post.blogName +
                        "<br> <b> Can like:</b> " + post.canLike +
                        "<br> <b> Can reblog: </b> " + post.canReblog +
                        "<br> <b> Can reply: </b> " + post.canReply +
                        "<br> <b> Can send in message: </b> " + post.canSendInMessage +
                        "<br> <b> Caption: </b> " + post.caption +
                        "<br> <b> Date: </b> " + post.date +
                        "<br> <b> Format: </b> " + post.format +
                        "<br> <b> Id: </b> " + post.id +
                        "<br> <b> Image permalink: </b> " + post.imagePermalink +
                        "<br> <b> Is blocks post format: </b> " + post.isBlocksPostFormat +
                        "<br> <b> Note count: </b> " + post.noteCount +
                        "<br> <b> Post url: </b> " + post.postUrl +
                        "<br> <b> Reblog key: </b> " + post.reblogKey +
                        "<br> <b> Recommended color: </b> " + post.recommendedColor +
                        "<br> <b> Recommended source: </b> " + post.recommendedColor +
                        "<br> <b> Short url: </b> " + post.shortUrl +
                        "<br> <b> Slug: </b> " + post.slug +
                        "<br> <b> State: </b> " + post.state +
                        "<br> <b> Summary: </b> " + post.summary +
                        "<br> <b> Tags: </b> " + tags +
                        "<br> <b> Timestamp: </b> " + DateFunction.getDateCurrentTimeZone(post.timestamp) +
                        "<br> <b> Type: </b> " + post.type;

                txtParacelableData.setText(Html.fromHtml(data));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        // set transition animation
        imgPostLarge.setTransitionName("simple_activity_transition");

        // get image link from intent
        Intent intent = getIntent();
        if (intent.getStringExtra("imgLink") == null) {
            Picasso.get().load(R.drawable.no_image_available).into(imgPostLarge);

        } else {

            Picasso.get().load(getIntent().getStringExtra("imgLink")).into(imgPostLarge, new Callback() {
                @Override
                public void onSuccess() {
                    supportStartPostponedEnterTransition();
                }

                @Override
                public void onError(Exception e) {
                    Picasso.get().load(R.drawable.no_image_available).into(imgPostLarge);
                }
            });
        }
    }

    private void setupToolbar() {
        // Add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
